heat.exe dir WinLua -nologo -srd -sreg -gg -cg ProjectDir -dr PROJECTLOCATION -var var.ProjectSourceDir -t exclude-files.xslt -out project-files.wxs
candle.exe -nologo -dProjectSourceDir=WinLua -arch x64 project-files.wxs winluajit.wxs
light.exe -nologo -ext WixUtilExtension -ext WixUIExtension -cultures:en-us -loc localization-en-us.wxl project-files.wixobj winluajit.wixobj -out winluajit.msi
