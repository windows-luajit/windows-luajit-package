@echo off
set PATH=%~dp0Lua\5.1\bin;%PATH%
set PATH=%~dp0LuaRocks;%PATH%
set PATH=%~dp0WLC\bin;%PATH%
set PATH=%~dp0XMake\bin;%PATH%
set LUA_PATH=;;%~dp0Lua\5.1\share\lua\5.1\?.lua;%APPDATA%\luarocks\share\lua\5.1\?.lua
set LUA_CPATH=;;%~dp0Lua\5.1\lib\lua\5.1\?.dll;%APPDATA%\luarocks\lib\lua\5.1\?.dll

title WinLuaJIT Command Prompt
"%WINLUAJIT_TOPDIR%Lua\5.1\bin\luajit.exe" %*
