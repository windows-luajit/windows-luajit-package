# Merge vcpkg-2021.05.12-installed.zip
New-Item WinLua\Lua\5.1\bin -ItemType Directory -Force
New-Item WinLua\Lua\5.1\lib -ItemType Directory -Force
New-Item WinLua\Lua\5.1\include -ItemType Directory -Force

Copy-Item installed\x64-windows\bin\*.dll WinLua\Lua\5.1\bin
Copy-Item installed\x64-windows\lib\*.lib WinLua\Lua\5.1\lib
Copy-Item installed\x64-windows\include\luajit210b3\*.h WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\tools\*.exe WinLua\Lua\5.1\bin
Copy-Item installed\x64-windows\include\*.h WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\libxml WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\lzma WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\openssl WinLua\Lua\5.1\include
Copy-Item WinLua\Lua\5.1\lib\libcrypto.lib WinLua\Lua\5.1\lib\libeay32.lib
Copy-Item WinLua\Lua\5.1\lib\libssl.lib WinLua\Lua\5.1\lib\ssleay32.lib

# Merge batch file
Copy-Item assets\winluajit-prompt.bat WinLua

# Copy legacy LuaRocks into Vendor
New-Item WinLua\Vendor -ItemType Directory -Force
Copy-Item luarocks-3.7.0-win32 WinLua\Vendor -Recurse -Force

# Copy legacy LuaRocks into Vendor
Copy-Item ..\luarocks\install.bat WinLua\Vendor\luarocks-3.7.0-win32

# Remove
Remove-Item WinLua\LibreSSL -Recurse -Force
Remove-Item WinLua\Lua\5.3 -Recurse -Force
Remove-Item WinLua\Lua\5.4 -Recurse -Force

Remove-Item WinLua\LuaRocks -Recurse -Force

