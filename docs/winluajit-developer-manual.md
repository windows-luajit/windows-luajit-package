---
title: WinLuaJIT開発者マニュアル
subject: WinLuaベースのLuaJIT環境構築説明書
date: 2021/06/14
author: 株式会社クリアコード
keywords: [WinLuaJIT]
---

# WinLuaJIT開発者マニュアル

WinLuaJITインストーラーのビルドにはいくつかビルド済みのバイナリが必要です。
本文書ではあらかじめビルド済みのバイナリからインストーラーを構築する方法を説明し、
より高度な手順として、インストーラーを最初から構築する手順を説明します。

## 前提条件

本文書で必要とするソフトウェアは以下のとおりです。

* Visual Studio 2017 Community Edition
* Wix Toolset v3.11.2
* zip/unzipコマンド(任意のアーカイバーで代用可)

またインストーラー及び同梱ソフトウェアの動作環境はWindows 10 (64bit)を想定しています。

## ビルド済みバイナリからインストーラーを構築する方法

インストーラーを構築するためWiX Toolsetを使用します。使用するバージョンは以下のとおりです。

* WiX Toolset v3.11.2

https://wixtoolset.org/releases/ からwix311-binaries.zipアーカイブをダウンロードし、
任意のディレクトリに展開し、パスを通しておく必要があります。

### ビルド済みバイナリのダウンロード

コンパイル済みのアーカイブをもとにインストーラーをビルドする方法を説明します。

* WinLua-rev2012a.zip (WinLuaのバイナリ一式)
* vcpkg-2021.05.12-installed.zip (LuaJITビルド済みバイナリ)
* luarocks-3.7.0-win32.zip (LuaRocksのWindows版バイナリ)
* luarocks-bundle.zip (バンドルするLuaRocksモジュールのバイナリ)

WinLua-rev2012a.zip および vcpkg-2021.05.12-installed.zip 、 loarocks-bundle.zip は
WinLuaJITのリリースページからダウンロードできます。

LuaRocksは http://luarocks.github.io/luarocks/releases/ からダウンロードできます。
legacy Windows packageをダウンロードしてください。(stand-alone版ではありません)

### windows-luajit-packageのチェックアウト

以降`C:\work\lua\windows-luajit-package`を例としてリポジトリをチェックアウトします。

```cmd
git clone https://gitlab.com/clear-code/windows-luajit-package/
```

### バンドルするファイル一式の展開

```cmd
cd msi
unzip WinLua-rev2012a.zip
unzip vcpkg-2021.05.12-installed.zip
unzip luarocks-3.7.0-win32.zip
unzip luarocks-bundle.zip
```

### インストール対象のファイルの準備

PowerShellで次のスクリプトを実行し、インストーラーに含めるファイルをWinLuaディレクトリ配下に集約します。

```powershell
PS C:\work\lua\windows-luajit-package\msi> mergefiles.ps1
```

### Wix Toolsetによるインストーラーのビルド

Wix Toolsetにパスが通った状態でmsi/build.batを実行します。

```cmd
C:\work\lua\windows-luajit-package\msi> build.bat
```

ビルドが正常終了すると、`winluajit.msi`が実行したバッチファイルと同じディレクトリに作成されます。

ビルド済みバイナリからインストーラーを構築する手順は以上です。

## バイナリをビルドしなおしてインストーラーを構築する方法

以下では、Visual Studio 2017 Community Editionを使用してバイナリをビルドしなおす方法を説明します。

### vcpkgによるLuaJITのビルド

LuaJITと依存するライブラリーをビルドするためには、[vcpkg](https://github.com/microsoft/vcpkg)の仕組みを使います。
windows-luajit-package/ports以下にLuaJITのvcpkg向けレシピがあるので、それを使ってビルドします。

最初にvcpkgの最新のリリースをチェックアウトします。(最新のリリースの`2021.05.12`を使います)

```cmd
git clone https://github.com/microsoft/vcpkg
git checkout 2021.05.12
```

次にwindows-luajit-package/ports以下をvcpkg/ports配下にコピーします。

最後にvcpkgを使ってLuaJITをビルドします。

```cmd
vcpkg install luajit210b3:x64-windows
```

上記を実行することで、LuaJITと依存するライブラリーが`vcpkg/installed`以下にインストールされます。
`installed`ディレクトリをzipでアーカイブしたものが`vcpkg-2021.05.12-installed.zip`です。

### バンドルするLuaJITのインストール

任意の場所にWinLuaのアーカイブを展開します。
(以下作業ディレクトリを`c:\work\lua\WinLua`とします。)

```cmd
cd c:\work\lua
unzip WinLua-rev2012a.zip
```

vcpkg-2021.05.12-installed.zip を展開し、バイナリをWinLuaにコピーします。

```powershell
New-Item WinLua\Lua\5.1\bin -ItemType Directory -Force
New-Item WinLua\Lua\5.1\lib -ItemType Directory -Force
New-Item WinLua\Lua\5.1\include -ItemType Directory -Force

Copy-Item installed\x64-windows\bin\*.dll WinLua\Lua\5.1\bin
Copy-Item installed\x64-windows\lib\*.lib WinLua\Lua\5.1\lib
Copy-Item installed\x64-windows\include\luajit210b3\*.h WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\tools\*.exe WinLua\Lua\5.1\bin
Copy-Item installed\x64-windows\include\*.h WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\libxml WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\lzma WinLua\Lua\5.1\include
Copy-Item installed\x64-windows\include\openssl WinLua\Lua\5.1\include
Copy-Item WinLua\Lua\5.1\lib\libcrypto.lib WinLua\Lua\5.1\lib\libeay32.lib
Copy-Item WinLua\Lua\5.1\lib\libssl.lib WinLua\Lua\5.1\lib\ssleay32.lib
```

### LuaRocksのインストール

luarocks-3.7.0-win32.zipを展開し、install.batをwindows-luajit-package/luarocks/install.batで上書きします。

その後、install.batを次のように実行し、LuaRocksをインストールします。

```powershell
.\luarocks-3.7.0-win32\install.bat /P "c:\work\lua\WinLua\LuaRocks" /LUA "c:\work\lua\WinLua\Lua\5.1" /NOADMIN /NOREG /Q /F
```

最後に環境変数を設定することで、LuaRocksのインストールは完了です。

```powershell
$env:PATH = "C:\work\lua\WinLua\Lua\5.1\bin;" + $env:PATH
$env:PATH = "C:\work\lua\WinLua\LuaRocks;" + $env:PATH
$env:PATH = "C:\work\lua\WinLua\WLC\bin;" + $env:PATH
$env:PATH = "C:\work\lua\WinLua\XMake\bin;" + $env:PATH
$env:PATH = "C:\work\vcpkg\vcpkg;" +$env:PATH
$env:LUA_PATH=";;c:\work\lua\WinLua\Lua\5.1\share\lua\5.1\?.lua"
$env:LUA_CPATH=";;c:\work\lua\WinLua\Lua\5.1\lib\lua\5.1\?.dll"
```

### Luaモジュールのインストール

次のようにして、あらかじめバンドルするLuaモジュールをインストールします。

```cmd
luarocks install inspect
luarocks install luasocket
luarocks install lua-cjson
luarocks install luaossl CRYPTO_INCDIR="c:\work\lua\WinLua\Lua\5.1\include" CRYPTO_LIBDIR="c:\work\lua\WinLua\Lua\5.1\lib" OPENSSL_INCDIR="c:\work\lua\WinLua\Lua\5.1\include" OPENSSL_LIBDIR="c:\work\lua\WinLua\Lua\5.1\lib"
```

インストールができたら、zipコマンドでアーカイブを作成します。

```cmd
zip -r luarocks-bundle.zip WinLua/Lua/5.1/lib/lua WinLua/Lua/5.1/lib/luarocks WinLua/Lua/5.1/share
```

ここまでで、ビルド済みバイナリ一式がすべて作成できました。
以降は「ビルド済みバイナリからインストーラーを構築する方法」を参照してください。
