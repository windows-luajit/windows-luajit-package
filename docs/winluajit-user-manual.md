---
title: WinLuaJITユーザーマニュアル
subject: WinLuaベースのLuaJIT環境説明書
date: 2021/06/14
author: 株式会社クリアコード
keywords: [WinLuaJIT]
---

# WinLuaJIT Installerについて

`WinLuaJIT Installer` は、 [WinLua](http://www.winlua.net/) をベースにした、LuaJITを使える環境をセットアップするツールです。
ベースとなっているのはWinLuaの提供するWinLua Installer Complete Toolchain Rev 2012aです。
`luajit.exe`、`luarocks.bat` を使って、Luaスクリプトを実行したり、Luaモジュールをインストールできます。

# 動作環境

* Windows 10 (64bit)

# 同梱ソフトウェア

| Software                            | Version             |
|-------------------------------------|---------------------|
| WinLua Installer Complete Toolchain | 2012a               |
| LuaJIT                              | 2.1.0 beta3 (64bit) |
| LuaRocks                            | 3.7.0               |
| inspect                             | 3.1.1-0             |
| lua-cjson                           | 2.1.0.6-1           |
| luaossl                             | 20200709-0          |
| luasocket                           | 3.0rc1-2            |

# インストール

最新のインストーラーを https://gitlab.com/clear-code/windows-luajit-package/ からダウンロードして.msiを実行します。
インストーラーではインストール先のみ変更することができます。
特に必要がなければ既定値の`C:\WinLuaJIT`にそのままインストールすることをおすすめします。

# アンインストール

「設定」の「アプリ」の「アプリと機能」から「WinLuaJIT」をアンインストールできます。
インストールしたフォルダー内にあるファイルがすべて削除されます。

# ライセンス

WinLuaJIT InstallerのライセンスはMITライセンスです。
詳細はLICENSEファイルを参照してください。

# Luaスクリプトの実行方法

インストール後にスタートーメニューから「WinLuaJIT Command Prompt」を実行すると、LuaJITを対話的に実行できます。
Ctrl+CでLuaJITの対話実行を終了できます。終了するとコマンドプロンプトに戻ります。
コマンドプロンプトではLuaのスクリプトを実行するのに必要な環境変数の設定があらかじめ行われています。

![WinLuaJIT prompt](winluajit-prompt.png)

あらかじめ設定してあるシステム環境変数は次のとおりです。

| Variable    | Value                                        |
|-------------|----------------------------------------------|
| `PATH`      | `C:\WinLuaJIT\Lua\5.1\bin`;                  |
|             | `C:\WinLuaJIT\LuaRocks`;                     |
|             | `C:\WInLuaJIT\WLC\bin`;                      |
|             | `C:\WInLuaJIT\XMake\bin`                     |
| `LUA_PATH`  | `;;C:\WinLuaJIT\Lua\5.1\share\lua\5.1\?.lua` |
| `LUA_CPATH` | `;;C:\WinLuaJIT\Lua\5.1\lib\lua\5.1\?.dll`   |

# Luaモジュールの管理

Luaモジュールの管理にはLuaRocksを使用します。

## Luaモジュールの確認方法

`luarocks list --tree=system`を実行するとあらかじめインストールされているLuaモジュールを確認できます。

```cmd
c:\WinLuaJIT> luarocks list --tree=system

Rocks installed for Lua 5.1 in c:/winluajit/lua/5.1
---------------------------------------------------

inspect
   3.1.1-0 (installed) - c:/winluajit/lua/5.1/lib/luarocks/rocks-5.1

lua-cjson
   2.1.0.6-1 (installed) - c:/winluajit/lua/5.1/lib/luarocks/rocks-5.1

luaossl
   20200709-0 (installed) - c:/winluajit/lua/5.1/lib/luarocks/rocks-5.1

luasocket
   3.0rc1-2 (installed) - c:/winluajit/lua/5.1/lib/luarocks/rocks-5.1

```

`--tree=system`を指定しない場合、ユーザーの`%APPDATA%`配下にインストールされたLuaモジュールの一覧を表示します。
そのため、インストール直後では何も表示されません。

```cmd
C:\WinLuaJIT>luarocks list

Rocks installed for Lua 5.1 in C:/Users/(ユーザー名)/AppData/Roaming/luarocks
-----------------------------------------------------------------------
```

## Luaモジュールのインストール方法

Luaモジュールを追加でインストールするには`luarocks install`を実行します。

```cmd
C:\WinLuaJIT>luarocks install luautf8

Installing https://luarocks.org/luautf8-0.1.3-1.src.rock

luautf8 0.1.3-1 depends on lua >= 5.1 (5.1-1 provided by VM)
wlc64.exe -O2 -c -o lutf8lib.o -IC:\WinLuaJIT\Lua\5.1\include lutf8lib.c
wlc64.exe -shared -o lua-utf8.dll lutf8lib.o C:\WinLuaJIT\Lua\5.1\lib/lua51.lib -lm
luautf8 0.1.3-1 is now installed in C:/Users/kenhys/AppData/Roaming/luarocks (license: MIT)
```

すると、Luaモジュールのダウンロードからインストールまで実行します。

制限事項として、Luaモジュールが対応しているコンパイラがVisual C++のみの場合、インストールができません。
これは、ベースとなっているWinLua由来の制限です。

## Luaモジュールのアンインストール方法

インストール済みのLuaモジュールを削除するには、`luarocks remove`を実行します。

```cmd
C:\WinLuaJIT>luarocks remove luautf8
Checking stability of dependencies in the absence of
luautf8 0.1.3-1...

Removing luautf8 0.1.3-1...
Removal successful.
```

なお、アンインストールできるのは、`%APPDATA%`配下にインストールしたLuaモジュールが対象です。


# トラブルシューティング

## Luaモジュールをインストールしようとしたら、`Error: Unknown protocol gitrec`が発生した


Gitコマンドをインストールした上で、`gitrec`をサポートするモジュールをあらかじめインストールしてください。

```
luarocks install luarocks-fetch-gitrec
```

## Luaモジュールをインストールしようとしたら、インストール済みのモジュールを参照してくれない


既定では`luarocks list`で見えている範囲(`%APPDATA%\luarocks`)のみで依存関係を解決します。
LuaJITのインストール先にインストールしてあるLuaモジュールを参照してインストールするには
次のうちどちらか望ましいかによって選択してください。

依存関係を解決してインストールするには次の2つのやりかたがあります。

* `luarocks installに--tree=system`を指定してインストールする
* `luarocks installに--deps-mode=all`を指定してインストールする

1.の`--tree=system`オプションを指定した場合、LuaJITのインストール先をみて依存関係を解決しようとします。
このとき、LuaJITのインストール先にモジュールをインストールしようとするので、管理者権限が必要になります。

2.の`--deps-mode=all`オプションをを指定すると、`%APPPDATA%\luarocks`だけでなく`luarocks list --system=tree`で見える範囲も参照して依存解決を行います。
依存先解決のときだけ参照するのでインストール時に管理者権限は不要です。
モジュールは`%APPDATA%\luarocks`へとインストールされます。
