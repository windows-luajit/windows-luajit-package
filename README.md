# WinLuaJIT Installer

The repository to build the installer which is based on [WinLua](http://www.winlua.net/).

# Documents

* [WinLuaJIT User Manual](docs/winluajit-user-manual.md)
* [WinLuaJIT Developer Manual](docs/winluajit-developer-manual.md)

# LICENSE

MIT. see [LICENSE](LICENSE) about details.
