local inspect = require("inspect")

local a = {1, 2}
local b = {3, 4, 5, a}
print(inspect(a))
print(inspect(b))

