local http = require "socket.http"
local log_file = require "logging.file"

local logger = log_file("test%s.log", "%Y-%m-%d")

logger:debug("This is debug log")
logger:error("This is error log")
